import Splash from './Splash';
import Login from './Login';
import Register from './Register';
import WelcomeAuth from './WelcomeAuth';

export {
    Login,
    Register,
    Splash,
    WelcomeAuth
}