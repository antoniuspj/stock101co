import React from 'react';
import { View, Text } from 'react-native';
import { colors } from '../../utils/colors';
import { Button } from '../../components';

const ActionButton = ({desc,title}) => {
    return(
        <View style={styles.wrapper.component}>
            <Text style={styles.text.desc}>{desc}</Text>
        <Button title={title}/>
        </View>
    );
}

const styles = {
    wrapper: {
        component:{
            marginBottom:43,
            maxWidth:220
        }
    },
    text: {
        desc:{
            textAlign:'center',
            marginBottom:6,
            paddingHorizontal:'15%',
            color: colors.text.default
        }
    }
}

export default ActionButton;