import React from 'react';
import { View, Text } from 'react-native';
import ActionButton from './ActionButton';
import { colors } from '../../utils/colors';

const WelcomeAuth = () => {
    return(
        <View style={styles.wrapper.page}>
            <View style={styles.wrapper.illustration} />
            <Text style={styles.text.welcome} >Welcome to 101.co</Text>
            <ActionButton desc="Silakan login terlebih dahulu" title="login" /> 
            <ActionButton desc="Silakan register jika belum punya akun" title="register"/>
        </View>
    );
}

const styles = {
    /* style will go here  */
    wrapper:{
        page:{
            flex:1,
            alignItems:'center',
            justifyContent:'center' 
        },
        illustration:{
            width:219, 
            height:117, 
            backgroundColor: colors.default,
            marginBottom:10
        }
    },
    text:{
        welcome:{
            fontSize:18, 
            fontWeight:'bold', 
            color: colors.text.default,
            marginBottom:76
        }
    }
}

export default WelcomeAuth;