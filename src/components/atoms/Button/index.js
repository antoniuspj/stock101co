import React from 'react';
import {Text} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {colors} from '../../../utils';

const Button = ({title}) =>{
    return(
        <TouchableOpacity style={styles.wrapper.component}>
            <Text style={styles.text.title}>{title}</Text>
        </TouchableOpacity>
    )
}

const styles = {
    wrapper:{
        component:{
            backgroundColor:colors.default,
            borderRadius:25
        }
    },
    text:{
        title:{
            color:'white', 
            fontWeight:'bold',
            fontSize:12,
            textTransform:'uppercase',
            textAlign:'center',
            paddingVertical:13
        }
    }
}

export default Button;